# Single Mirantis cluster environment

Create a single Mirantis environment.

## Introduction

This config can be is used for experimenting with a Mirantis single cluster.
It also van be used for setting up a mirantis single- or multinode cluster.
You need Virtual Box and Vagrant for the installation of the virtual machines:

https://www.vagrantup.com/downloads.html

https://www.virtualbox.org/wiki/Downloads

## Installing the Boxes

To install the boxes, do the following:

```console
git clone https://dennisvermeulen@bitbucket.org/dennisvermeulen/mirantis-cluster.git
cd mirantis-cluster/
mirantis-cluster % vagrant up
mirantis-cluster % vagrant ssh vm1
[vagrant@cinqmke ~]$ cd ansible-mke-msr-wor/
[vagrant@cinqmke ansible-mke-msr-wor]$ ansible-playbook -b -K -i vm_hosts site.yml
BECOME password: vagrant
```

## Check url

login with username and password:

https://192.168.33.70/login/
https://192.168.33.71/login/

